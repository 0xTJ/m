#ifndef INCLUDE_M_HPP
#define INCLUDE_M_HPP

#include <array>
#include <iterator>
#include <ranges>

namespace M {
    template <class T, std::size_t N>
    struct MatrixRow : public std::array<T, N> {
        using array_type = std::array<T, N>;

        constexpr array_type::reference operator()(array_type::size_type pos) {
            return array_type::operator[](pos);
        }

        constexpr array_type::const_reference operator()(array_type::size_type pos) const {
            return array_type::operator[](pos);
        }
    };

    template <class T, std::size_t M, std::size_t N, bool IsVector = false> requires(M > 0 && N > 0)
    struct Matrix : public std::array<MatrixRow<T, N>, M> {
        using array_type = std::array<MatrixRow<T, N>, M>;
        using base_array_type = std::conditional_t<IsVector, MatrixRow<T, N>, std::array<MatrixRow<T, N>, M>>;

        // TODO: There has to be a better way of doing this, find it
        template <class Iter>
        using ColVectorIterBase = std::reverse_iterator<std::reverse_iterator<Iter>>;
        template <class Iter>
        struct ColVectorIter : ColVectorIterBase<Iter> {
            using iterator_type = Iter;
            using value_type = std::iterator_traits<Iter>::value_type::value_type;
            using difference_type = std::iter_difference_t<Iter>;
            using pointer = std::conditional_t<
                std::is_const_v<std::remove_pointer_t<typename ColVectorIterBase<Iter>::pointer>>,
                typename std::iterator_traits<Iter>::value_type::const_pointer,
                typename std::iterator_traits<Iter>::value_type::pointer>;
            using reference = std::conditional_t<
                std::is_const_v<std::remove_reference_t<typename ColVectorIterBase<Iter>::reference>>,
                typename std::iterator_traits<Iter>::value_type::const_reference,
                typename std::iterator_traits<Iter>::value_type::reference>;

            constexpr explicit ColVectorIter(Iter ptr) :
                ColVectorIterBase<Iter>(std::make_reverse_iterator(std::make_reverse_iterator(ptr))) {}

            constexpr iterator_type base() const = delete;

            constexpr reference operator*() {
                return ColVectorIterBase<Iter>::operator*()[0];
            }

            constexpr pointer operator->() {
                return &(ColVectorIterBase<Iter>::operator*()[0]);
            }

            constexpr reference operator[](difference_type n) {
                return ColVectorIterBase<Iter>::operator[](n)[0];
            }
        };

        using value_type = base_array_type::value_type;
        using size_type = base_array_type::size_type;
        using difference_type = base_array_type::difference_type;
        using reference = base_array_type::reference;
        using const_reference = base_array_type::const_reference;
        using pointer = base_array_type::pointer;
        using const_pointer = base_array_type::const_pointer;
        using iterator = std::conditional_t<IsVector && N == 1, 
            ColVectorIter<typename array_type::iterator>,
            typename base_array_type::iterator>;
        using const_iterator = std::conditional_t<IsVector && N == 1, 
            ColVectorIter<typename array_type::const_iterator>,
            typename base_array_type::const_iterator>;
        using reverse_iterator = std::conditional_t<IsVector && N == 1, 
            std::reverse_iterator<iterator>,
            typename base_array_type::reverse_iterator>;
        using const_reverse_iterator = std::conditional_t<IsVector && N == 1, 
            std::reverse_iterator<const_iterator>,
            typename base_array_type::const_reverse_iterator>;

        // Element access

        constexpr reference at(size_type pos) {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::at(0).at(pos);
                } else if constexpr (N == 1) {
                    return array_type::at(pos).at(0);
                }
            } else {
                return array_type::at(pos);
            }
        }
        constexpr const_reference at(size_type pos) const {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::at(0).at(pos);
                } else if constexpr (N == 1) {
                    return array_type::at(pos).at(0);
                }
            } else {
                return array_type::at(pos);
            }
        }

        constexpr reference operator[](size_type pos) {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0)[pos];
                } else if constexpr (N == 1) {
                    return array_type::operator[](pos)[0];
                }
            } else {
                return array_type::operator[](pos);
            }
        }
        constexpr const_reference operator[](size_type pos) const {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0)[pos];
                } else if constexpr (N == 1) {
                    return array_type::operator[](pos)[0];
                }
            } else {
                return array_type::operator[](pos);
            }
        }

        constexpr array_type::reference operator()(size_type pos) {
            return array_type::operator[](pos);
        }
        constexpr array_type::const_reference operator()(size_type pos) const {
            return array_type::operator[](pos);
        }

        constexpr reference front() {
            if constexpr (IsVector) {
                return array_type::front().front();
            } else {
                return array_type::front();
            }
        }
        constexpr const_reference front() const {
            if constexpr (IsVector) {
                return array_type::front().front();
            } else {
                return array_type::front();
            }
        }

        constexpr reference back() {
            if constexpr (IsVector) {
                return array_type::back().back();
            } else {
                return array_type::back();
            }
        }
        constexpr const_reference back() const {
            if constexpr (IsVector) {
                return array_type::back().back();
            } else {
                return array_type::back();
            }
        }

        // Iterators

        constexpr iterator begin() noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).begin();
                } else if constexpr (N == 1) {
                    return iterator(array_type::begin());
                }
            } else {
                return array_type::begin();
            }
        }
        constexpr const_iterator begin() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).begin();
                } else if constexpr (N == 1) {
                    return const_iterator(array_type::begin());
                }
            } else {
                return array_type::begin();
            }
        }
        constexpr const_iterator cbegin() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).cbegin();
                } else if constexpr (N == 1) {
                    return const_iterator(array_type::cbegin());
                }
            } else {
                return array_type::cbegin();
            }
        }

        constexpr iterator end() noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).end();
                } else if constexpr (N == 1) {
                    return iterator(array_type::end());
                }
            } else {
                return array_type::end();
            }
        }
        constexpr const_iterator end() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).end();
                } else if constexpr (N == 1) {
                    return const_iterator(array_type::end());
                }
            } else {
                return array_type::end();
            }
        }
        constexpr const_iterator cend() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).cend();
                } else if constexpr (N == 1) {
                    return const_iterator(array_type::cend());
                }
            } else {
                return array_type::cend();
            }
        }

        constexpr reverse_iterator rbegin() noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).rbegin();
                } else if constexpr (N == 1) {
                    return reverse_iterator(array_type::rbegin());
                }
            } else {
                return array_type::rbegin();
            }
        }
        constexpr const_reverse_iterator rbegin() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).rbegin();
                } else if constexpr (N == 1) {
                    return const_reverse_iterator(array_type::rbegin());
                }
            } else {
                return array_type::rbegin();
            }
        }
        constexpr const_reverse_iterator crbegin() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).crbegin();
                } else if constexpr (N == 1) {
                    return const_reverse_iterator(array_type::crbegin());
                }
            } else {
                return array_type::crbegin();
            }
        }

        constexpr reverse_iterator rend() noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).rend();
                } else if constexpr (N == 1) {
                    return reverse_iterator(array_type::rend());
                }
            } else {
                return array_type::rend();
            }
        }
        constexpr const_reverse_iterator rend() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).rend();
                } else if constexpr (N == 1) {
                    return const_reverse_iterator(array_type::rend());
                }
            } else {
                return array_type::rend();
            }
        }
        constexpr const_reverse_iterator crend() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).crend();
                } else if constexpr (N == 1) {
                    return const_reverse_iterator(array_type::crend());
                }
            } else {
                return array_type::crend();
            }
        }

        // Capacity

        [[nodiscard]] constexpr bool empty() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).empty();
                } else if constexpr (N == 1) {
                    return array_type::empty();
                }
            } else {
                return array_type::empty();
            }
        }

        constexpr size_type size() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).size();
                } else if constexpr (N == 1) {
                    return array_type::size();
                }
            } else {
                return array_type::size();
            }
        }

        constexpr size_type max_size() const noexcept {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    return array_type::operator[](0).max_size();
                } else if constexpr (N == 1) {
                    return array_type::max_size();
                }
            } else {
                return array_type::max_size();
            }
        }

        // Operations

        constexpr void fill(const T& value) {
            if constexpr (IsVector) {
                if constexpr (M == 1) {
                    array_type::operator[](0).fill(value);
                } else if constexpr (N == 1) {
                    array_type::fill(value);
                }
            } else {
                for (auto &row : *this) {
                    row.fill(value);
                }
            }
        }

        // Inherit swap()
    };

    template <class T, std::size_t N>
    using RowVector = Matrix<T, 1, N, true>;

    template <class T, std::size_t N>
    using ColVector = Matrix<T, N, 1, true>;

    template <class T, std::size_t N>
    using SqrMatrix = Matrix<T, N, N, false>;

    ////////////////
    // Assignment //
    ////////////////

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr Matrix<T, M, N, IsVector>& operator+=(Matrix<T, M, N, IsVector> &a, const Matrix<T, M, N, IsVector>& b) {
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                a(i)(j) += b(i)(j);
            }
        }
        return a;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr Matrix<T, M, N, IsVector>& operator-=(Matrix<T, M, N, IsVector>& a, const Matrix<T, M, N, IsVector>& b) {
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                a(i)(j) -= b(i)(j);
            }
        }
        return a;
    }

    template <class T, std::size_t N>
    constexpr SqrMatrix<T, N>& operator*=(SqrMatrix<T, N>& a, const SqrMatrix<T, N>& b) {
        a = a * b;
        return a;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector, class TScalar>
    constexpr Matrix<T, M, N, IsVector>& operator*=(Matrix<T, M, N, IsVector>& a, const TScalar& b) {
        a = a * b;
        return a;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector, class TScalar>
    constexpr Matrix<T, M, N, IsVector>& operator/=(Matrix<T, M, N, IsVector>& a, const TScalar& b) {
        a = a / b;
        return a;
    }

    ////////////////
    // Arithmetic //
    ////////////////

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr auto operator+(const Matrix<T, M, N, IsVector>& a) {
        return a;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr auto operator-(const Matrix<T, M, N, IsVector>& a) {
        Matrix<T, M, N, IsVector> result;
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                result(i)(j) = -a(i)(j);
            }
        }
        return result;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr auto operator+(const Matrix<T, M, N, IsVector>& a, const Matrix<T, M, N, IsVector>& b) {
        Matrix<T, M, N, IsVector> c;
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                c(i)(j) = a(i)(j) + b(i)(j);
            }
        }
        return c;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector>
    constexpr auto operator-(const Matrix<T, M, N, IsVector>& a, const Matrix<T, M, N, IsVector>& b) {
        Matrix<T, M, N, IsVector> c;
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                c(i)(j) = a(i)(j) - b(i)(j);
            }
        }
        return c;
    }

    template <class TA, class TB, std::size_t M, std::size_t N, std::size_t P, bool IsVectorA, bool IsVectorB>
    constexpr auto operator*(const Matrix<TA, M, N, IsVectorA>& a, const Matrix<TB, N, P, IsVectorB>& b) {
        Matrix<decltype(TA{} * TB{}), M, P, IsVectorA || IsVectorB> c{};
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, P)) {
                for (auto k : std::views::iota(std::size_t{0}, N)) {
                    c(i)(j) += a(i)(k) * b(k)(j);
                }
            }
        }
        return c;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector, class TScalar>
    constexpr auto operator*(const TScalar& a, const Matrix<T, M, N, IsVector>& b) {
        Matrix<T, M, N, IsVector> c;
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                c(i)(j) = a * b(i)(j);
            }
        }
        return c;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector, class TScalar>
    constexpr auto operator*(const Matrix<T, M, N, IsVector>& a, const TScalar& b) {
        return b * a;
    }

    template <class T, std::size_t M, std::size_t N, bool IsVector, class TScalar>
    constexpr auto operator/(const Matrix<T, M, N, IsVector>& a, const TScalar& b) {
        Matrix<T, M, N, IsVector> c;
        for (auto i : std::views::iota(std::size_t{0}, M)) {
            for (auto j : std::views::iota(std::size_t{0}, N)) {
                c(i)(j) = a(i)(j) / b;
            }
        }
        return c;
    }

    template <class TA, std::size_t MA, std::size_t NA, class TB, std::size_t MB, std::size_t NB, bool IsVectorA, bool IsVectorB>
    constexpr auto operator^(const Matrix<TA, MA, NA, IsVectorA> &a, const Matrix<TB, MB, NB, IsVectorB> &b) {
        Matrix<decltype(std::declval<TA>() * std::declval<TB>()), MA * MB, NA * NB, IsVectorA && IsVectorB> c;
        for (auto r : std::views::iota(std::size_t{0}, MA)) {
            for (auto s : std::views::iota(std::size_t{0}, NA)) {
                for (auto v : std::views::iota(std::size_t{0}, MB)) {
                    for (auto w : std::views::iota(std::size_t{0}, NB)) {
                        c(MB * r + v)(NB * s + w) = a(r)(s) * b(v)(w);
                    }
                }
            }
        }
        return c;
    }
}

#endif
