#include "M.hpp"
#include <complex>
#include <iostream>
#include <string>

using namespace std;
using namespace M;

Matrix<double, 2, 2> test1;
const ColVector<int, 2> test2 = {};
const RowVector<int, 2> test3 = {};
const ColVector<std::string, 3> test4 = {"ds"};
Matrix<std::complex<double>, 4, 4> test5{{{
                { 1.0, 0.0, 0.0, 0.0 },
                { 0.0, 0.0, 1.0, 0.0 },
                { 0.0, 1.0, 0.0, 0.0 },
                { 0.0, 0.0, 0.0, 1.0 },
            }}};

int main() {
    test1 *= 2;
    test1 * test2;

    for (auto &x : test1) {
        for (auto &y : x) {
            cout << y << '\n';
        }
    }

    for (auto &x : test2) {
        cout << x << '\n';
    }

    cout << test4.begin()->size() << '\n';
    for (const auto &row : test5) {
        for (const auto &item : row) {
            cout << item << "\n";
        }
    }

    for (auto &x : test3) {
        cout << x << '\n';
    }
}
